function [WarpCost, Path] = dtw(ref, test, varargin)
% [WarpCost, Path] = dtw(ref, test, OptionalArgs)
% Compute the dynamic time warp cost between ref and test.
% ref and test must be matrices where each row is a feature
% vector.
% A WarpCost of Inf is returned when the signals cannot be
% warped to one another.
%
% ref - Reference pattern
% test - Test pattern to be warped to reference
% Multidimensional signals are expected to be column-oriented; NxM
% where N is the signal length and M is the number of feautres.
%
% Optional arguments
% 'DistortionFn' -
%   Either:
%   A Function handle (@) to compute distortion between
%   any a row of ref and a row of test.
%   or
%   A number, in which case the distance between points is raised
%    to the number specified.  e.g. 2 --> L2 distortion.
%    Even numbers are of course recommended.
%  If not specified, L2 distortion is used
% 'Visualize' true|false - Generate plots to visualize the warping.
%   The first plot shows both the point to point distortions as well
%   as the cumulative distortion and the best path.
% 'Constraints', scalar or cell array
%   Scalar - Specifies which set of valid moves are possible as per
%       Rabiner & Juang (1993).  Currently, only constraint set
%       5 has been implmeented, adding others is trivial.
%   CellArray - Each element is a matrix describing the offsets
%       from the current point along with a possible weighting.  See
%       code for details.
% 'SampleRateHz', N - sample rate of feature vectors in Hz (e.g. 100
%       features per second), only useful with Visualize to provide
%       time axis in seconds.
% 'ComponentLabels', cell_array - Provide labels for each component.
%       Only useful when Visualize is true.  Example:
%       'ComponentLabels', {'Z', 'dZ'} or {'c0', 'c1', 'c2'}
%       Must be of same length as dimension of signals to be warped.
%
% Author:  Marie A. Roch e-mail first-name.last-name@sdsu.edu
%          Kait Frasier  e-mail firstInitialLastName@ucsd.edu
%          2013-2018
%
% References:
%
% Rabiner, L. R. and B.-H. Juang (1993).
% Fundamentals of speech recognition,
% Englewood Cliffs, NJ 07632, Prentice-Hall, pp. 204-229.
%
% and
%
% Myers, C., L. R. Rabiner, et al. (1980).
% "Performance tradeoffs in dynamic time warping algorithms
% for isolated word recognition."
% IEEE Trans Acoust., Speech, Signal Processing 28(6): 623-635.

% defaults
distortionFn = [];
L = 2; % L2 distortion
visualize = false;
time_units = 'samples';  % for time axis
constraints = 5;  % Possible paths allowed
SampleRateHz = 1;
ComponentLabels = {};  
Loc = 'southeast';   % legend location

% Make one dimensionsal row signals column oriented
if size(ref, 1) == 1 && size(ref, 2) > 1
    ref = ref';
end
if size(test, 1) == 1 && size(test, 2) > 1
    test = test';
end

vidx = 1;
while vidx < length(varargin)
    switch varargin{vidx}
        case 'DistortionFn'
            arg = varargin{vidx+1}; vidx = vidx + 2;
            if isa(arg, 'function_handle')
                distortionFn = arg;
                L = [];
            elseif isscalar(arg)
                L = arg;
            else
                error('DistortionFn not a function handle or scalar');
            end
        case 'Constraints'
            constraints = varargin{vidx+1}; vidx = vidx + 2;
            if ~isscalar (constraints) && ~iscell(constraints)
                error('Constraints must be a scalar or cell array');
            end
        case 'Visualize'
            visualize = varargin{vidx+1}; vidx = vidx + 2;
        case 'SampleRate'
            SampleRateHz = varargin{vidx+1}; vidx = vidx + 2;
            time_units = 's';
        case 'ComponentLabels'
            ComponentLabels = varargin{vidx+1}; vidx = vidx + 2;
            assert(iscell(ComponentLabels), ...
                'ComponentLabels must be a cell');
            assert(length(ComponentLabels) == size(Ref, 2), ...
                'ComponentLabels must have same number of dimensions as signals');
        otherwise
            error('Bad optional argument');
    end
end

% For internal purposes, the reference can be thought of as
% being on the vertical axis and the test vector on the
% horizontal.

% Path constraints
% Each path consists of offsets from the current point back to
% the cumulative distortion.  Some paths contain multiple steps.
% Each step is weighted with a scale factor.
% Example:  [-1 -1  m   %   cumdist(tmpidx-1,refidx-1)*m
%             0  0  n]  %   + distortion(tmpidx-0, refidx-0)*n
%
%     o   o   o   o <-- current point: r,t
%                ^
%               /
%              /
%     o   o   o   o
%             ^---  r-1, t-1 (row 1 in example)


if isscalar(constraints)
    % User specified a number for constraint set.
    switch constraints
        case 1
            constraints = {
                % Type I constraints, see p. 223 Rabiner & Juang 1993
                [-1  0  1
                0  0  1]
                [-1 -1  2
                0  0  1]
                [ 0 -1  1
                0  0  1]
                };
        case 2
            constraints = {
                [-2 -1 .5
                -1  0 .5
                0  0  1]
                [-1 -1  1
                0  0  1]
                [-1 -2 .5
                0 -1 .5
                0  0  1]
                };
        case 5
            constraints = {
                % Type V constraints, see p. 223 Rabiner & Juang 1993
                [-3 -1  1
                -2  0  2
                -1  0  1
                0  0  1]
                [-1 -1  1
                -1  0  2
                0  0  1]
                [-1 -1  1
                0  0  2]
                [-1 -2  1
                0 -1  2
                0  0  1]
                [-1 -3  1
                0 -2  2
                0 -1  1
                0  0  1]
                };
        otherwise
            error('Bad constraint set');
    end
end

% Number of elements in each path component
HopsN = zeros(size(constraints, 1));
for idx=1:length(HopsN)
    HopsN(idx) = size(constraints{idx}, 1);
end

% Find maximum/minimum expansion
% First entry of each constraint is the farthest from the point
% that will be under current consideration.  It also contains
% the maximal distance from that point, so we can take the ratio
% of the movement along each axis.
Qmax = max(cellfun(@(p) p(1,1)/p(1,2), constraints));
Qmin = min(cellfun(@(p) p(1,1)/p(1,2), constraints));

testN = size(test, 1);
refN = size(ref, 1);

Path = [];  % Warp path
ratio = refN/testN;
if ratio < Qmin || ratio > Qmax
    % too short/long
    WarpCost = Inf;
    return
end

% Compute distortions
d = inf(refN, testN);
gridP = zeros(refN, testN);

% Anonymous function calls have a performance hit.
% This was originally designed to use the nest L2 distortion
% function, but it saves a bit of time if we duplicate the code
% and inline the function when L2 is needed.
if ~isempty(L)
    for ridx = 1:refN
        for tidx = 1:testN
            % Check outgoing/incoming constraints
            % This restricts our search space to a parallelogram
            % We could refine this by incorporating Sakoe and Chiba's
            % global path constraints.
            %
            % Changed /Qmax should be *Qmin, I suspect that that
            % Myers et al. assumed that QMin = 1/QMax as that
            % is usually the case, but doesn't have to be.
            outgoingP = 1+(tidx-1)*Qmin <= ridx && ridx <= 1+Qmax*(tidx-1);
            incomingP = refN+Qmax*(tidx-testN) <= ridx && ...
                ridx <= refN+(tidx-testN)*Qmin;
            gridP(ridx, tidx) = outgoingP && incomingP;
            if gridP(ridx, tidx)
                % Compute L_i distortion (e.g. L2 or Euclidean)
                d(ridx, tidx) = sum((ref(ridx,:) - test(tidx,:)) .^ L);
            end
        end
    end
else
    for ridx = 1:refN
        for tidx = 1:testN
            % Check outgoing/incoming constraints
            % This restricts our search space to a parallelogram
            % We could refine this by incorporating Sakoe and Chiba's
            % global path constraints.
            %
            % Changed /Qmax should be *Qmin, I suspect that that
            % Myers et al. assumed that QMin = 1/QMax as that
            % is usually the case, but doesn't have to be.
            outgoingP = 1+(tidx-1)*Qmin <= ridx && ridx <= 1+Qmax*(tidx-1);
            incomingP = refN+Qmax*(tidx-testN) <= ridx && ...
                ridx <= refN+(tidx-testN)*Qmin;
            gridP(ridx, tidx) = outgoingP && incomingP;
            if gridP(ridx, tidx)
                d(ridx, tidx) = distortionFn(ref(ridx,:),test(tidx,:));
            end
        end
    end
end





% warpspeed essentialy does the following:
% D = inf(refN, testN);  % Cumulative distance matrix
% D(1,1) = d(1,1);
% backtrace = zeros(refN, testN);
% 
% cost = zeros(length(constraints), 1);  % Cost for each constraint path
% costN = length(cost);
%
% for tidx = 2:testN
%     % Find first point of reference vector to which we might
%     % be able to warp
%     rfirst = find(gridP(:, tidx), 1, 'first');
%     for ridx = rfirst:refN
%         if gridP(ridx, tidx)
%             % Find lowest cost path
%             for cidx = 1:costN
%                 % Look at each element of constraint path
%                 
%                 % See if path will be out of bounds
%                 if ridx+constraints{cidx}(1,1) < 1 || tidx+constraints{cidx}(1,2) < 1
%                     cost(cidx) = Inf;
%                     continue;
%                 else
%                     % Cumulative distance at start point
%                     cost(cidx) = D(ridx + constraints{cidx}(1,1), tidx + constraints{cidx}(1,2));
%                     % Loop through remaining elements of constraint path
%                     for eidx=2:HopsN(cidx)
%                         % Weighted cost of moving to each point
%                         cost(cidx) = cost(cidx) + ...
%                             d(ridx+constraints{cidx}(eidx,1),tidx+constraints{cidx}(eidx,2)) * ...
%                             constraints{cidx}(eidx,3);
%                     end
%                 end
%             end
%             % Store the cumulative cost and the rule that
%             % was used to get us there.
%             [D(ridx, tidx), backtrace(ridx, tidx)] = min(cost);
%         else
%             % Moved off parallelogram, no point burning cycles
%             % looking for points we'll never compare
%             continue
%         end
%     end
% end
[D,backtrace] = warpspeed(d, gridP, constraints);


% Best path
[WarpCost, ridx] = min(D(:,testN));

if isinf(WarpCost)
    % Warping failed
    Path = [];
else
    if nargout > 1 || visualize
        % Extract the warp path
        tidx = testN;
        LongestPossiblePath = ceil(testN * Qmax);
        Path = zeros(LongestPossiblePath, 2);
        Path(end,:) = [ridx, tidx];
        pathidx = LongestPossiblePath - 1;
        while tidx > 1
            % Work backwards through current rule, recovering path
            % We never get to the very first element of the rule
            % as that is the current point.
            rule = constraints{backtrace(ridx, tidx)};
            cidx = size(rule, 1)-1;
            while cidx > 0
                Path(pathidx, :) = [ridx+rule(cidx,1), tidx+rule(cidx,2)];
                pathidx = pathidx - 1;
                cidx = cidx - 1;  % Next rule
            end
            % Prepare for next
            ridx = ridx + rule(1, 1);
            tidx = tidx + rule(1, 2);
        end
        % Remove unused path components
        Path(1:pathidx, :) = [];
    end
    
    if visualize

        interval = 1/SampleRateHz;  % interval between samples
        
        % Show the unwarped and warped versions
        figure('Name', 'Signals - Upper: left unwarped, right warped Bottom: path')
        PathN = size(Path, 1);
        Dim = size(ref, 2);
            
        % subplot axes, two component of feature vector (unwarped/warped)
        % and final warp path
        ax = zeros(2*Dim+1, 1);
        diVal = 1;
        
        % Differences between plotting reference and test signals
        gray = [.4 .4 .4];
        props{1} = {'MarkerSize', 5', 'MarkerFaceColor', 'k'};
        props{2} = {'Color', gray, 'MarkerSize', 5, ...
                    'MarkerFaceColor', gray};
        fontsize = {'FontSize', 12};
        
        % time axes
        t{1} = [0:refN-1]*interval;
        t{2} = [0:testN-1]*interval;
        t{3} = [0:PathN-1]*interval;  % warp path length
        
        for component = 1:Dim
            % subplot(rows, cols, posn)
            % posn 1 is r1,c1, 2 is r1, c2, etc.
            posn = (component-1)*2+1;
            % Unwarped
            ax(component) = subplot(Dim+1, 2, posn);
            % plot ref & test and retain the handles
            h = plot(t{1}, ref(:, component), '-k', ...
                     t{2}, test(:, component), '--k', 'LineWidth', 3);
            for idx=1:2
                set(h(idx), props{idx}{:});
            end
            set(ax(component), fontsize{:});
            legend(h, {'Contour A', 'Contour B'},'Location',Loc);
            
            if component == Dim
                xlabel(time_units);
            end
            if ~ isempty(ComponentLabels)
                ylabel(ComponentLabels{component});
            else
                ylabel(sprintf('component %d', component))
            end
            
            % Warped
            ax(component+Dim) = subplot(Dim+1, 2, posn+1);
            h = plot(t{3}, ref(Path(:,1), component), '-k', ...
                t{3}, test(Path(:,2), component), '--k', 'LineWidth', 3);
            for idx=1:2
                set(h(idx), props{idx}{:});
            end
            set(ax(component+Dim), fontsize{:});
            legend(h, {'Contour A', 'Contour B'},'Location',Loc);
            if component == Dim
                xlabel(time_units);
            end            
            
        end
        linkaxes(ax(1:2*Dim), 'x');  % Lock zoom on both subplots
        
        % Last plot takes two columns 
        ax(2*Dim+1) = subplot(Dim+1, 2, 2*Dim+[1 2]);

        colormap('gray')
        imagesc(t{1}, t{2}, D');
        set(gca, fontsize{:});
        hc = colorbar;
        set(gca, 'YDir', 'normal')
        xlabel('Contour B','FontSize',14);
        ylabel('Contour A','FontSize',14)
        hold on
        % Show warp path on cumulative distortion plot
        plot(Path(:,1).*interval, Path(:,2).*interval, ...
            'wo-','linewidth',2);
        hold off
        1;
    end
end
