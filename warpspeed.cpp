#include <mex.h>
#include <matrix.h>
#include <limits>

// #define DEBUG	1

#ifdef DEBUG
#include <cstdio>
#endif
/*
 * Given a pointer to the base of an array, determine the pointer to 
 * an element of a 2D column-major array when the row size is
 * as specified
 */
inline double * access(double *baseptr, size_t row, size_t col, size_t rowSize) {
  return baseptr + col*rowSize + row;
}

/*
 * [D, backtrace] = warpspeed(d, gridP, constraints)
 * Implements a Matlab bottleneck for dynamic time warping in C++/C
 * Inputs:
 * d - distortion between points
 * gridP - predicate for pruning off parallelagram
 * constraints - Cell array of constraints, see dtw for details.
 * Outputs:
 * D - Cumulative distortion matrix  D(i,j) is the cost of the lowest path
 *	   to i,j
 * backtrace - indices pointing back to recover path
 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	if (nlhs != 2)
	  mexErrMsgTxt(
	    "Must provide output variables for cumulative distortion matrix and backtrace");
	if (nrhs != 3)
	  mexErrMsgTxt("Right hand side needs (d, gridP, constraints");

	double infinity = mxGetInf();

	const mxArray *dmat = prhs[0];

	// Find length of reference and test signals being warped
	const size_t refN = mxGetM(dmat);
	const size_t  testN = mxGetN(dmat);


	const mxArray *GridPMat = prhs[1];
	const mxArray *constraintsMat = prhs[2];

	if (mxIsCell(constraintsMat) == false)
		mexErrMsgTxt("constraints argument must be a cell array");

	// Pointer to data for feature vector comparisions
	double *d = mxGetPr(dmat);

	// Initialize outputs
	
	// Create cumulative distortion matrix
	mxArray *Dmat = mxCreateDoubleMatrix(refN, testN, mxREAL);
	plhs[0] = Dmat;
	// Pointer to data for cumulative distortion matrix
	double *D = mxGetPr(Dmat);	// cumulative distortion
	// Initialize warp path
	*D = *d;	// Copy d[0][0] to D[0][0]
	// Set everything else to infinity
	for (size_t idx = 1; idx < refN * testN; idx++)
		D[idx] = infinity;

	// Create backtrace structure.  backtrace[ref][test] gives the index
	// of the ref row in the 
	mxArray *backtraceMat = mxCreateDoubleMatrix(refN, testN, mxREAL);
	plhs[1] = backtraceMat;

	// TODO:  Some more sanity checks
	// Verify sizes identical for GridPMat, dMat, DMat
	// Verify constraints is a cell array with appropriate entries...


	// predicate for whether vector pairs lie within permissible 
	// global constraints, do the reference/test points lie within
	// the parallelogram
	double *gridP = mxGetPr(GridPMat);	
	double *backtrace = mxGetPr(backtraceMat);
	
	size_t CellRows = mxGetM(constraintsMat);
	size_t CellCols = mxGetN(constraintsMat);
	size_t  constraintsN = CellRows * CellCols;
	// Allocate pointers to N doubles
	// and double arrays to store the sizes
	double **constraints = new double * [constraintsN];
	size_t *constraintsRows = new size_t[constraintsN];
	size_t *constraintsCols = new size_t[constraintsN];
	// cost of each constraint (calculate later)
	double *costs = new double[constraintsN];
	/*
	 * Learn the size of each of the constraints that were
	 * passed in and get a pointer to the data 
	 */
	for (int cidx = 0; cidx  < constraintsN; cidx++) {
	  mxArray *entry = mxGetCell(constraintsMat, cidx);
	  constraintsRows[cidx] = mxGetM(entry);
	  constraintsCols[cidx] = mxGetN(entry);	  
	  constraints[cidx] = mxGetPr(entry);
	}
	


	// Remember that Matlab stores data in column-major
	// order while C/C++ uses row-major order.  
	// The access function allows us to access arrays
	// stored in column major order.

#ifdef DEBUG
	FILE *dbg = fopen("warpspeed_debug.txt", "w");
	if (dbg == NULL)
		mexErrMsgTxt("compiled with DEBUG option and unable to open DEBUG file in current directory");
	fprintf(dbg, "refN %zd testN %zd\n", refN, testN);
#endif

	for (size_t tidx = 1; tidx < testN; tidx++) {
	  // Find start of each column
	  double *Dcol = access(D, 0, tidx, refN);
	  double *dcol = access(d, 0, tidx, refN);
	  double *gridPcol = access(gridP, 0, tidx, refN);
	  double *backtracecol = access(backtrace, 0, tidx, refN);
#ifdef DEBUG
	  fprintf(dbg, "\ntidx=%zd \n", tidx);
	  fflush(dbg);
#endif


	  for (size_t ridx = 0; ridx < refN; ridx++) {
	    if (gridPcol[ridx]) {
		  // On search grid, does not violate global constraints established
		  // by calling code.
#ifdef DEBUG
		  fprintf(dbg, "\n\tridx=%zd ", ridx);
		  fflush(dbg);
#endif
	      
	      // loop through constraints, seeing if their origin
	      // is still on the grid
	      double mincost = infinity;
	      double mincostidx = -1;

	      for (size_t cidx = 0; cidx < constraintsN; cidx++) {
#ifdef DEBUG
			fprintf(dbg, "cidx = %zd ", cidx);
			fflush(dbg);
#endif
			// First offset in constraint path is the largest,
			// see if it takes us off the grid.
			double *constraint = constraints[cidx];
			size_t crows  = constraintsRows[cidx];
			size_t col1 = crows; // offset to start column 1
			size_t col2 = crows + crows;  // offset to start of column 2
			if (ridx + constraint[0] < 0 ||
				tidx + constraint[crows] < 0) 
				costs[cidx] = infinity;
			else {
				// Find cumulative distortion to oldest entry in
				// path.
				costs[cidx] = *access(D, ridx+static_cast<size_t>(constraint[0]),
					tidx+static_cast<size_t>(constraint[col1]),
					refN);
				//* static_cast<size_t>(constraint[col2]);
				for (int eidx=1; eidx < crows; eidx++) {
					// Check each of the remaining edges, adding in
					// the pair-wise distoriton scaled by the cost
					costs[cidx] +=
					*access(d, ridx+static_cast<size_t>(constraint[eidx]),
					tidx+static_cast<size_t>(constraint[eidx+col1]), refN)
					* constraint[eidx+col2];
			  
					}
				if (costs[cidx] < mincost) {
					// new lower cost
					mincost = costs[cidx];
					mincostidx = cidx;
				}
			}
	      }
		  // All edges processed, keep best one
		  *(Dcol + ridx) = mincost;
		  *(backtracecol + ridx) = mincostidx + 1;
	    }
	  }
	}
#ifdef DEBUG
	fprintf(dbg, "\nReturn\n");
	fflush(dbg);
    fclose(dbg);
#endif
	return;
}
