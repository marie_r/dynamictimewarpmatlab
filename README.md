# Dynamic time warping code

Implementation of dynamic time warping in Matlab.  
Bottleneck poritons are coded in C++ and must be compiled.
See Matlab documentation for supported compilers.  Once a compiler
is set up, compile from Matlab with:

mex warpspeed.cpp


